import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class MarketEvent extends Equatable {}

class MarketStarted extends MarketEvent {
  @override
  List<Object> get props => throw UnimplementedError();
}

// Load by active tab
class MarketTabChanged extends MarketEvent {
  final String currentTab;

  MarketTabChanged({this.currentTab});

  @override
  List<Object> get props => [currentTab];
}

// Load data by market name
class FetchMarketData extends MarketEvent {
  final String exchangeName;

  FetchMarketData({this.exchangeName});

  @override
  List<Object> get props => [exchangeName];
}

class MarketPriceIncremented extends MarketEvent {
  @override
  List<Object> get props => [];
}

class MarketPriceDecremented extends MarketEvent {
  @override
  List<Object> get props => [];
}
