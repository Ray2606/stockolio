class Result<T> {
  bool success;
  T data;
  String message;

  Result({this.success = true, this.data, this.message});
}
