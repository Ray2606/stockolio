import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:stockolio/bloc/profile/profile_event.dart';
import 'package:stockolio/bloc/profile/profile_state.dart';
import 'package:stockolio/repository/profile_repository/profile_repo.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  ProfileRepository profileRepository;
  ProfileBloc({@required this.profileRepository});
  @override
  ProfileState get initialState => ProfileInitial();

  @override
  Stream<ProfileState> mapEventToState(ProfileEvent event) async* {
    if (event is ProfileStarted) {
      yield ProfileFetchDataInprogress();
    }

    if (event is ProfileFetchData) {
      var data = await profileRepository.getSymbolInfo(event.symbol);

      if (data != null)
        yield ProfileFetchDataSuccess(profileData: data, isSaved: true);
      else
        yield ProfileFetchDataFailure(errMsg: 'Get symbol error');
    }
  }
}
