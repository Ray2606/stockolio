import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';

import 'package:stockolio/widgets/common/header.dart';

// Header screen watchlist
class PortfolioHeadingSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final String formattedDate = DateFormat('MMMMd').format(DateTime.now());

    return StandardHeader(
      title: 'Danh mục',
      subtitle: formattedDate,
      action: GestureDetector(
          child: Icon(FontAwesomeIcons.user),
          onTap: () => Navigator.pushNamed(context, '/about')),
    );
  }
}
