import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:stockolio/bloc/technical/technical_event.dart';
import 'package:stockolio/bloc/technical/technical_state.dart';
import 'package:stockolio/repository/market_repo/market_repo.dart';

class TechnicalBloc extends Bloc<TechnicalEvent, TechnicalState> {
  MarketRepository marketRepository;

  TechnicalBloc({@required this.marketRepository});

  @override
  TechnicalState get initialState => TechnicalInitial();

  @override
  Stream<TechnicalState> mapEventToState(TechnicalEvent event) async* {
    if (event is TechnicalStarted) yield TechnicalFetchChartDataInProgress();

    if (event is TechnicalFetchChartData) {
      var chartResult = await marketRepository.fetchHoubiTradingData('1day');

      if (chartResult.success) {
        yield TechnicalFetchChartDataSuccess(kLineData: chartResult.data);
      } else {
        yield TechnicalFetchChartDataFailure(errMsg: chartResult.message);
      }
    }
  }
}
