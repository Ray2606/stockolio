import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:stockolio/bloc/technical/technical_bloc.dart';
import 'package:stockolio/bloc/technical/technical_event.dart';
import 'package:stockolio/bloc/technical/technical_state.dart';
import 'package:stockolio/library/k_chart_library/flutter_k_chart.dart';
import 'package:stockolio/library/k_chart_library/k_chart_widget.dart';

import 'package:http/http.dart' as http;
import 'package:stockolio/model/market/stock_quote.dart';
import 'package:stockolio/repository/market_repo/market_repo.dart';
import 'package:stockolio/shared/color_helper.dart';
import 'package:stockolio/shared/colors.dart';
import 'package:stockolio/shared/styles.dart';
import 'package:stockolio/shared/text_helper.dart';
import 'package:stockolio/widgets/common/empty_screen.dart';
import 'package:stockolio/widgets/common/loading_indicator.dart';

class ChartDetail extends StatefulWidget {
  final StockQuote stockQuote;

  ChartDetail({Key key, @required this.stockQuote}) : super(key: key);

  @override
  _ChartDetailState createState() => _ChartDetailState();
}

class _ChartDetailState extends State<ChartDetail> {
  List<KLineEntity> datas;
  bool showLoading = true;
  MainState _mainState = MainState.MA;
  bool _volHidden = false;
  SecondaryState _secondaryState = SecondaryState.MACD;
  bool isLine = false;
  bool isVietnamese = false;
  List<DepthEntity> _bids, _asks;
  StockQuote stkQuote;

  @override
  void initState() {
    super.initState();
    // getData('1day');
    rootBundle.loadString('assets/depth.json').then((result) {
      final parseJson = json.decode(result);
      Map tick = parseJson['tick'];
      var bids = tick['bids']
          .map((item) => DepthEntity(item[0], item[1]))
          .toList()
          .cast<DepthEntity>();
      var asks = tick['asks']
          .map((item) => DepthEntity(item[0], item[1]))
          .toList()
          .cast<DepthEntity>();
      initDepth(bids, asks);
    });
  }

  void initDepth(List<DepthEntity> bids, List<DepthEntity> asks) {
    if (bids == null || asks == null || bids.isEmpty || asks.isEmpty) return;
    _bids = List();
    _asks = List();
    double amount = 0.0;
    bids?.sort((left, right) => left.price.compareTo(right.price));
    //累加买入委托量
    bids.reversed.forEach((item) {
      amount += item.vol;
      item.vol = amount;
      _bids.insert(0, item);
    });

    amount = 0.0;
    asks?.sort((left, right) => left.price.compareTo(right.price));
    //累加卖出委托量
    asks?.forEach((item) {
      amount += item.vol;
      item.vol = amount;
      _asks.add(item);
    });
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    stkQuote = widget.stockQuote;
    return Scaffold(
      backgroundColor: kScaffoldBackground, //Color(0xff17212F),
      appBar: AppBar(
        title: Text(
          this.stkQuote.name,
          style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
          ),
        ),
        backgroundColor: kPositiveColor,
      ),
      body: BlocProvider(
          create: (context) =>
              TechnicalBloc(marketRepository: MarketRepository()),
          child: BlocBuilder<TechnicalBloc, TechnicalState>(
              builder: (context, state) {
            if (state is TechnicalInitial) {
              BlocProvider.of<TechnicalBloc>(context)
                  .add(TechnicalFetchChartData(symbol: stkQuote.symbol));
            }

            if (state is TechnicalFetchChartDataInProgress)
              return LoadingIndicatorWidget();

            if (state is TechnicalFetchChartDataSuccess) {
              datas = state.kLineData;
              showLoading = false;
              // setState(() {});
              return buildNormalContent(this.stkQuote);
            }

            if (state is TechnicalFetchChartDataFailure) {
              showLoading = false;
              setState(() {});
              return EmptyScreen(
                message: state.errMsg,
              );
            }

            return EmptyScreen(
              message: 'no data',
            );
          })),
    );
  }

  Widget buildContentWithHeaderSliver() {
    return NestedScrollView(
      headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
        return <Widget>[
          SliverAppBar(
            backgroundColor: kPositiveColor,
            expandedHeight: 150.0,
            floating: false,
            pinned: true,
            title: Text(
              this.stkQuote.name,
              style: TextStyle(
                color: Colors.white,
                fontSize: 16.0,
              ),
            ),
            flexibleSpace: FlexibleSpaceBar(
              title: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  // price and change percent
                  Row(
                    children: [
                      Text('\$${formatText(this.stkQuote.price)}',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16.0,
                          )),
                      Text(
                          '${determineTextBasedOnChange(this.stkQuote.change)}  (${determineTextPercentageBasedOnChange(this.stkQuote.changesPercentage)})',
                          style: determineTextStyleBasedOnChange(
                              this.stkQuote.change)),
                    ],
                  ),
                  // high, low, vol
                  Row(),
                ],
              ),
            ),
          ),
        ];
      },
      body: ListView(
        children: <Widget>[
          Stack(children: <Widget>[
            Container(
              height: 450,
              width: double.infinity,
              child: KChartWidget(
                datas,
                isLine: isLine,
                mainState: _mainState,
                volHidden: _volHidden,
                secondaryState: _secondaryState,
                fixedLength: 2,
                timeFormat: TimeFormat.YEAR_MONTH_DAY,
                isVietnamese: isVietnamese,
              ),
            ),
            if (showLoading)
              Container(
                  width: double.infinity,
                  height: 450,
                  alignment: Alignment.center,
                  child: CircularProgressIndicator()),
          ]),
          buildButtons(),
          Container(
            height: 230,
            width: double.infinity,
            child: DepthChart(_bids, _asks),
          )
        ],
      ),
    );
  }

  Widget buildNormalContent(StockQuote stkQuote) {
    return ListView(
      children: <Widget>[
        Container(
          height: 100,
          // color: kPositiveColor,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              // price and change percent
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('${formatText(stkQuote.price)}',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                      )),
                  Text(
                      '${determineTextBasedOnChange(stkQuote.change)}  (${determineTextPercentageBasedOnChange(stkQuote.changesPercentage)})',
                      style: determineTextStyleBasedOnChange(stkQuote.change)),
                ],
              ),
              // high, low, vol
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text('High: ${formatText(stkQuote.price)}',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                      )),
                  Text('Low: ${formatText(stkQuote.price)}',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                      )),
                  Text('Volume: ${formatText(stkQuote.volume)}',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                      )),
                ],
              ),
            ],
          ),
        ),
        Container(
          height: 30,
          // color: Colors.grey,
          child: Row(
            children: [
              expandedButton('1H', onPressed: () => {}),
              expandedButton('4H', onPressed: () => {}),
              expandedButton('1D', onPressed: () => {}),
              expandedButton('1W', onPressed: () => {}),
              Expanded(
                child: DropdownButton(
                  items: [
                    DropdownMenuItem(
                      key: Key('1m'),
                      value: '1m',
                      child: Text(
                        '1m',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    DropdownMenuItem(
                      key: Key('15m'),
                      value: '15m',
                      child: Text(
                        '15m',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    DropdownMenuItem(
                      key: Key('30m'),
                      value: '30m',
                      child: Text(
                        '30m',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    DropdownMenuItem(
                      key: Key('6H'),
                      value: '6H',
                      child: Text(
                        '6H',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    DropdownMenuItem(
                      key: Key('12H'),
                      value: '12H',
                      child: Text(
                        '12H',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    )
                  ],
                  onChanged: (String value) {},
                ),
              ),
              expandedButton('Line', onPressed: () => isLine = true),
            ],
          ),
        ),
        Stack(children: <Widget>[
          Container(
            height: 450,
            padding: EdgeInsets.only(top: 10),
            width: double.infinity,
            child: KChartWidget(
              datas,
              isLine: isLine,
              mainState: _mainState,
              volHidden: _volHidden,
              secondaryState: _secondaryState,
              fixedLength: 2,
              timeFormat: TimeFormat.YEAR_MONTH_DAY,
              isVietnamese: isVietnamese,
            ),
          ),
          if (showLoading)
            Container(
                width: double.infinity,
                height: 450,
                alignment: Alignment.center,
                child: CircularProgressIndicator()),
        ]),
        buildTechnicalButton(),
        // buildButtons(),
        Container(
          height: 230,
          width: double.infinity,
          child: DepthChart(_bids, _asks),
        )
      ],
    );
  }

  Widget buildTechnicalButton() {
    return Row(
      children: [
        expandedButton('MA', onPressed: () => _mainState = MainState.MA),
        expandedButton('BOLL', onPressed: () => _mainState = MainState.BOLL),
        expandedButton('MACD',
            onPressed: () => _secondaryState = SecondaryState.MACD),
        expandedButton('RSI',
            onPressed: () => _secondaryState = SecondaryState.RSI),
        expandedButton('KDJ',
            onPressed: () => _secondaryState = SecondaryState.KDJ),
        expandedButton('WR',
            onPressed: () => _secondaryState = SecondaryState.WR)
      ],
    );
  }

// Button mặc định của library
  Widget buildButtons() {
    return Wrap(
      alignment: WrapAlignment.spaceEvenly,
      children: <Widget>[
        button("Line", onPressed: () => isLine = true),
        button("Candle", onPressed: () => isLine = false),
        button("MA", onPressed: () => _mainState = MainState.MA),
        button("BOLL", onPressed: () => _mainState = MainState.BOLL),
        button("Remove main indicator",
            onPressed: () => _mainState = MainState.NONE),
        button("MACD", onPressed: () => _secondaryState = SecondaryState.MACD),
        button("KDJ", onPressed: () => _secondaryState = SecondaryState.KDJ),
        button("RSI", onPressed: () => _secondaryState = SecondaryState.RSI),
        button("WR", onPressed: () => _secondaryState = SecondaryState.WR),
        button("Remove second indicator",
            onPressed: () => _secondaryState = SecondaryState.NONE),
        button(_volHidden ? "Hide volume" : "UnHide volume",
            onPressed: () => _volHidden = !_volHidden),
      ],
    );
  }

  Widget expandedButton(String text, {VoidCallback onPressed}) {
    return Expanded(
      child: FlatButton(
        onPressed: () {
          if (onPressed != null) {
            onPressed();
            setState(() {});
          }
        },
        child: Text(
          "$text",
          style: TextStyle(fontSize: 12),
        ),
      ),
    );
  }

  Widget button(String text, {VoidCallback onPressed}) {
    return FlatButton(
        onPressed: () {
          if (onPressed != null) {
            onPressed();
            setState(() {});
          }
        },
        child: Text("$text"),
        color: Colors.blue);
  }

  void getData(String period) {
    Future<String> future = getIPAddress('$period');
    future.then((result) {
      Map parseJson = json.decode(result);
      List list = parseJson['data'];
      datas = list
          .map((item) => KLineEntity.fromJson(item))
          .toList()
          .reversed
          .toList()
          .cast<KLineEntity>();
      DataUtil.calculate(datas);
      showLoading = false;
      setState(() {});
    }).catchError((_) {
      showLoading = false;
      setState(() {});
      print('Get huobi data error');
    });
  }

  //Hàm lấy dữ liệu từ sàn Huobi
  Future<String> getIPAddress(String period) async {
    var url =
        'https://api.huobi.br.com/market/history/kline?period=1day&size=100&symbol=btcusdt';
    // var url =
    // 'https://api.huobi.br.com/market/history/kline?period=${period ?? '1day'}&size=10&symbol=btcusdt';
    String result;
    var response = await http.get(url);
    if (response.statusCode == 200) {
      result = response.body;
    } else {
      print('Failed getting IP address');
      result = sampleDataStr;
    }
    return result;
  }

  String sampleDataStr =
      '{"data":[{"id":1605024000,"open":15168.02,"close":15396.9,"low":15112.15,"high":15500,"amount":12364.421682618293,"vol":189843513.61103123,"count":174428},{"id":1604937600,"open":15004.24,"close":15168.03,"low":14813,"high":15491.34,"amount":29847.28646129053,"vol":455357989.1401526,"count":388377},{"id":1604851200,"open":15354.61,"close":15004.24,"low":14934.26,"high":15850,"amount":46707.575707467135,"vol":721192238.2556374,"count":466990},{"id":1604764800,"open":15342.64,"close":15354.61,"low":14366,"high":15450.01,"amount":40030.645315910864,"vol":599354138.1559778,"count":464329},{"id":1604678400,"open":15408.46,"close":15342.64,"low":15180,"high":15753.88,"amount":27811.906619509642,"vol":430796623.4177301,"count":363819},{"id":1604592000,"open":15076.96,"close":15411.23,"low":14800,"high":15985,"amount":68162.9676460744,"vol":1055177027.6322238,"count":709617},{"id":1604505600,"open":13911.96,"close":15077.18,"low":13886.64,"high":15150,"amount":54586.13821699717,"vol":788043048.5249326,"count":577493},{"id":1604419200,"open":13708.92,"close":13911.95,"low":13524.7,"high":14058,"amount":36143.3015509102,"vol":498599440.220474,"count":390022},{"id":1604332800,"open":13492.15,"close":13708.92,"low":13286.26,"high":13799,"amount":30005.983998813892,"vol":406257058.5598601,"count":364009},{"id":1604246400,"open":13797.97,"close":13492.15,"low":13192.98,"high":13863.23,"amount":36300.605196519,"vol":492648664.3052716,"count":394442}]}';
}
