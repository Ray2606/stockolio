part of 'portfolio_bloc.dart';

@immutable
abstract class PortfolioEvent extends Equatable {}

class FetchPortfolioData extends PortfolioEvent {
  @override
  List<Object> get props => throw UnimplementedError();
}

class SaveProfile extends PortfolioEvent {
  @override
  List<Object> get props => throw UnimplementedError();
  // final StorageModel storageModel;

  // SaveProfile({@required this.storageModel});
}

class DeleteProfile extends PortfolioEvent {
  final String symbol;

  DeleteProfile({@required this.symbol});

  @override
  List<Object> get props => [symbol];
}
