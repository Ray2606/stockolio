import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:stockolio/bloc/portfolio/portfolio_bloc.dart';
import 'package:stockolio/shared/colors.dart';
import 'package:stockolio/widgets/common/empty_screen.dart';
import 'package:stockolio/widgets/portfolio/widgets/portfolio_content.dart';

import 'widgets/portfolio_heading.dart';

// Man hinh danh sach watchlist
class PortfolioSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kScaffoldBackground,
      appBar: PreferredSize(
          child: SafeArea(
            child: Padding(
              padding: EdgeInsets.only(
                left: 16,
                right: 16,
              ),
              child: PortfolioHeadingSection(),
            ),
          ),
          preferredSize: Size.fromHeight(65)),
      body: OfflineBuilder(
          child: Container(),
          connectivityBuilder: (
            context,
            connectivity,
            child,
          ) {
            return connectivity == ConnectivityResult.none
                ? _buildNoConnectionMessage(context)
                : _buildContent(context);
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: () => {},
        child: const Icon(Icons.add),
        backgroundColor: Colors.white,
      ),
    );
  }

  Widget _buildNoConnectionMessage(context) {
    return Padding(
      padding: EdgeInsets.only(
          top: MediaQuery.of(context).size.height / 14, left: 24, right: 24),
      child: EmptyScreen(
          message: 'Looks like you don\'t have an internet connection.'),
    );
  }

  Widget _buildContent(context) {
    return RefreshIndicator(
      child: SafeArea(
        child: ListView(
          // disabled scroll refresh
          // physics: BouncingScrollPhysics(),
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
          children: [
            PortfolioContentSection(),
          ],
        ),
      ),
      onRefresh: () async {
        // Reload stocks section.
        BlocProvider.of<PortfolioBloc>(context).add(FetchPortfolioData());
      },
    );
  }
}
