import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class TechnicalEvent extends Equatable {}

class TechnicalStarted extends TechnicalEvent {
  @override
  List<Object> get props => [];
}

// Event trigger load chart_data of specified symbol
class TechnicalFetchChartData extends TechnicalEvent {
  final String symbol;
  TechnicalFetchChartData({@required this.symbol});
  @override
  List<Object> get props => [symbol];
}
