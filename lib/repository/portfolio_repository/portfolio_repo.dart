import 'package:stockolio/model/market/stock_overview.dart';
import 'package:stockolio/model/sampleData.dart';

class PortfolioRepository {
  Future<List<StockOverviewModel>> watchList() {
    return Future.delayed(
        Duration(milliseconds: 1000), () => SampleData.getWatchList());
  }

  Future<bool> saveToWatchList(String symbol) {
    return Future.delayed(Duration(microseconds: 500));
  }
}
