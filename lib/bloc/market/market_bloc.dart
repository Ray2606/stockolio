import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stockolio/model/sampleData.dart';
import 'package:stockolio/repository/market_repo/market_repo.dart';

import 'market_state.dart';
import 'market_event.dart';

class MarketBloc extends Bloc<MarketEvent, MarketState> {
  final MarketRepository marketRepository;
  // final SharedPreferences localStorageManager;

  MarketBloc({@required this.marketRepository});
  @override
  MarketState get initialState => MarketInitial();

  @override
  Stream<MarketState> mapEventToState(MarketEvent event) async* {
    final SharedPreferences localStorageManager =
        await SharedPreferences.getInstance();
    yield MarketFetchDataInprogress();

    // khi tab changed, tim vao local storage xem da co du lieu chua, neu chua co thi load moi
    if (event is MarketTabChanged) {
      if (localStorageManager.containsKey(event.currentTab)) {
        var exchangeCodeData =
            localStorageManager.getStringList(event.currentTab);

        var exchangeData =
            SampleData.getExchangeSymbolByCodes(exchangeCodeData);

        yield MarketFetchDataSuccess(data: exchangeData);
      } else {
        this.add(FetchMarketData(exchangeName: event.currentTab));
      }
    }

    if (event is FetchMarketData) {
      var exchangeSymbols =
          await marketRepository.getMarketDataByExchange(event.exchangeName);

      if (exchangeSymbols != null) {
        if (exchangeSymbols.length > 0) {
          var symbolCodes = exchangeSymbols.map((e) => e.code).toList();

          localStorageManager.setStringList(event.exchangeName, symbolCodes);
        }
        yield MarketFetchDataSuccess(data: exchangeSymbols);
      } else {
        yield MarketFetchDataFailure(
            errMsg:
                'Load market data of exchange ${event.exchangeName} failure!!');
      }
    }
  }
}
