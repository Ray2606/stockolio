class ExchangeModel {
  String code, name, curency;
  DateTime atoStart,
      atoClose,
      amContinuousStart,
      amContinuousEnd,
      pmContinuousStart,
      pmContinuousEnd,
      atcStart,
      atcClose;
  double mktVolume, mktCap;

  ExchangeModel(
      {this.code,
      this.name,
      this.curency,
      this.atoStart,
      this.atoClose,
      this.amContinuousStart,
      this.amContinuousEnd,
      this.pmContinuousStart,
      this.pmContinuousEnd,
      this.atcStart,
      this.atcClose,
      this.mktCap,
      this.mktVolume});

  factory ExchangeModel.fromJson(Map<String, dynamic> json) {
    return ExchangeModel(
      code: json['code'],
      name: json['name'],
      curency: json['curency'],
    );
  }
}
