import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:stockolio/model/market/stock_overview.dart';
import 'package:stockolio/repository/portfolio_repository/portfolio_repo.dart';

part 'portfolio_event.dart';
part 'portfolio_state.dart';

class PortfolioBloc extends Bloc<PortfolioEvent, PortfolioState> {
  final PortfolioRepository portfolioRepository;

  PortfolioBloc({@required this.portfolioRepository});

  @override
  PortfolioState get initialState => PortfolioInitial();

  @override
  Stream<PortfolioState> mapEventToState(PortfolioEvent event) async* {
    yield PortfolioLoading();

    if (event is FetchPortfolioData) {
      var watchList = await portfolioRepository.watchList();

      if (watchList != null) {
        yield PortfolioLoadSuccess(watchList: watchList);
      } else {
        yield PortfolioLoadFailure(message: 'Fetch portfolio data failure!');
      }
    }
  }

  Stream<PortfolioState> _loadContent() async* {
    // try {
    //   final symbolsStored = await _databaseRepository.fetch();
    //   final indexes = await _repository.fetchIndexes();

    //   if (symbolsStored.isNotEmpty) {
    //     final stocks = await Future.wait(symbolsStored.map((symbol) async =>
    //         await _repository.fetchStocks(symbol: symbol.symbol)));

    //     yield PortfolioLoaded(stocks: stocks, indexes: indexes);
    //   } else {
    //     yield PortfolioStockEmpty(indexes: indexes);
    //   }
    // } catch (e, stack) {
    //   yield PortfolioError(
    //       message: 'There was an unkwon error. Please try again later.');
    //   // await SentryHelper(exception: e, stackTrace: stack).report();
    // }
  }
}
