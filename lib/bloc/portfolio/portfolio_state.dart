part of 'portfolio_bloc.dart';

@immutable
abstract class PortfolioState extends Equatable {}

class PortfolioInitial extends PortfolioState {
  @override
  List<Object> get props => throw UnimplementedError();
}

class PortfolioLoadFailure extends PortfolioState {
  final String message;

  PortfolioLoadFailure({@required this.message});

  @override
  List<Object> get props => [message];
}

class PortfolioLoading extends PortfolioState {
  @override
  List<Object> get props => throw UnimplementedError();
}

class PortfolioLoadSuccess extends PortfolioState {
  final List<StockOverviewModel> watchList;

  PortfolioLoadSuccess({@required this.watchList});

  @override
  List<Object> get props => [watchList];
}
