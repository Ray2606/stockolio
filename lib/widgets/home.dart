import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:stockolio/shared/colors.dart';

import 'market/markets.dart';
import 'portfolio/portfolio.dart';

class StockolioAppHome extends StatefulWidget {
  @override
  _StockolioAppHomeState createState() => _StockolioAppHomeState();
}

class _StockolioAppHomeState extends State<StockolioAppHome> {
  int _selectedIndex = 0;

  final List<Widget> tabs = [
    PortfolioSection(),
    MarketsSection(),
    PortfolioSection(),
    PortfolioSection(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: kScaffoldBackground,
        body: tabs.elementAt(_selectedIndex),
        bottomNavigationBar: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 8),
            child: GNav(
                gap: 12,
                activeColor: Colors.teal,
                iconSize: 24,
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                duration: Duration(milliseconds: 400),
                tabBackgroundColor: Colors.teal[50],
                selectedIndex: _selectedIndex,
                tabs: _bottomNavigationBarItemItems(),
                onTabChange: _onItemTapped),
          ),
        ));
  }

  List<GButton> _bottomNavigationBarItemItems() {
    return [
      GButton(
        // icon: FontAwesomeIcons.home,
        icon: Icons.account_balance_wallet,
        text: 'Danh mục',
      ),
      GButton(
        // icon: FontAwesomeIcons.store,
        icon: Icons.store,
        text: 'Thị trường',
      ),
      // GButton(
      //   icon: FontAwesomeIcons.search,
      //   // icon: Icons.search,
      //   text: 'Search',
      // ),
      GButton(
        // icon: FontAwesomeIcons.solidNewspaper,
        icon: Icons.library_books,
        text: 'Tin tức',
      ),
      GButton(
        // icon: FontAwesomeIcons.cog,
        icon: Icons.settings,
        text: 'Cài đặt',
      )
    ];
  }

  void _onItemTapped(int index) {
    setState(() => _selectedIndex = index);
  }
}
