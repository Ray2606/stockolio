class StockOverviewModel {
  String code, name, exchangeCode;
  int price, changeAmount;
  double changePercent;

  StockOverviewModel(
      {this.code,
      this.name,
      this.exchangeCode,
      this.price,
      this.changePercent,
      this.changeAmount});

  factory StockOverviewModel.fromJson(Map<String, dynamic> json) {
    return StockOverviewModel(
        code: json['code'],
        name: json['name'],
        exchangeCode: json['exchangeCode'],
        price: json['price'],
        changePercent: json['changePercent'],
        changeAmount: json['changeAmount']);
  }
}
