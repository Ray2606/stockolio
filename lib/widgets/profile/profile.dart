import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:stockolio/bloc/profile/profile_bloc.dart';
import 'package:stockolio/bloc/profile/profile_state.dart';
import 'package:stockolio/shared/color_helper.dart';
import 'package:stockolio/shared/colors.dart';
import 'package:stockolio/widgets/common/empty_screen.dart';
import 'package:stockolio/widgets/common/loading_indicator.dart';
import 'package:stockolio/widgets/profile/profile_screen.dart';

// Man hinh xem chi tiet 1 co phieu
class Profile extends StatelessWidget {
  final String symbol;

  Profile({@required this.symbol});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
        builder: (BuildContext context, ProfileState state) {
      if (state is ProfileFetchDataFailure) {
        return Scaffold(
            appBar: AppBar(
              backgroundColor: kNegativeColor,
              title: Text(':('),
            ),
            backgroundColor: kScaffoldBackground,
            body: Center(child: EmptyScreen(message: state.errMsg)));
      }

      if (state is ProfileFetchDataSuccess) {
        return ProfileScreen(
          isSaved: state.isSaved,
          profile: state.profileData,
          color:
              determineColorBasedOnChange(state.profileData.stockInfo.changes),
        );
      }

      return Scaffold(
          backgroundColor: kScaffoldBackground, body: LoadingIndicatorWidget());
    });
  }
}
