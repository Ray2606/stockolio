import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:stockolio/bloc/portfolio/portfolio_bloc.dart';
import 'package:stockolio/model/market/stock_overview.dart';
import 'package:stockolio/widgets/common/empty_screen.dart';
import 'package:stockolio/widgets/common/loading_indicator.dart';
import 'package:stockolio/widgets/portfolio/widgets/portfolio_stock_card.dart';

class PortfolioContentSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // ignore: close_sinks
    final _portfolioBloc = BlocProvider.of<PortfolioBloc>(context);
    return BlocBuilder<PortfolioBloc, PortfolioState>(
        builder: (context, state) {
      if (state is PortfolioInitial) {
        _portfolioBloc.add(FetchPortfolioData());
      }

      if (state is PortfolioLoadFailure) {
        return Padding(
          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height / 3),
          child: EmptyScreen(message: state.message),
        );
      }

      if (state is PortfolioLoadSuccess) {
        return _buildStocksSection(stocks: state.watchList);
      }

      return Padding(
        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height / 3),
        child: LoadingIndicatorWidget(),
      );
    });
  }

  Widget _buildStocksSection({List<StockOverviewModel> stocks}) {
    return ListView.builder(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        itemCount: stocks.length,
        itemBuilder: (BuildContext context, int index) {
          return PortfolioStockCard(data: stocks[index]);
        });
  }
}
