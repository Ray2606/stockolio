import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:stockolio/bloc/market/market_bloc.dart';
import 'package:stockolio/bloc/portfolio/portfolio_bloc.dart';
import 'package:stockolio/bloc/profile/profile_bloc.dart';
import 'package:stockolio/repository/market_repo/market_repo.dart';
import 'package:stockolio/repository/portfolio_repository/portfolio_repo.dart';
import 'package:stockolio/repository/profile_repository/profile_repo.dart';
import 'package:stockolio/widgets/home.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(MultiBlocProvider(
      providers: [
        BlocProvider<PortfolioBloc>(
          create: (context) =>
              PortfolioBloc(portfolioRepository: PortfolioRepository()),
        ),
        BlocProvider<MarketBloc>(
            create: (context) =>
                MarketBloc(marketRepository: MarketRepository())),
        BlocProvider<ProfileBloc>(
            create: (context) =>
                ProfileBloc(profileRepository: ProfileRepository()))
      ],
      child: MaterialApp(
        title: 'STOCKOLIO',
        theme: ThemeData(brightness: Brightness.dark),
        home: StockolioAppHome(),
        debugShowCheckedModeBanner: false,
        routes: {'/about': (context) => StockolioAppHome()},
      )));
}
