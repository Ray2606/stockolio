import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StorageManager {
  static final secureStorage = new FlutterSecureStorage();

  static void saveData(String dataKey, val) async =>
      await secureStorage.write(key: dataKey, value: val);

  static Future<dynamic> readKey(String dataKey) async =>
      await secureStorage.read(key: dataKey);
}
